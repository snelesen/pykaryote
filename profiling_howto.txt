Profiling How-to

This document describes how to profile pykaryote code.

First enable cython profiling by adding this complier directive to the
top of all .pyx files:
	# cython: profile=True

Then recompile pykaryote using:
	python setup.py build_ext --inplace

To profile a comparison using the default config files, run:
	python profile.py

This will run the simulation and save profiling statistics in 
'tests/profile/Profile.prof'
To analyze profile data interactively, run:
	python -m pstats tests/profile/Profile.prof

You will be presented with a prompt. Use these commands to sort by runtime
and print the top 10 functions:
	sort time
	stats 10

For more help, use these commands:
	help
	help sort
	help stats

For a blog post describing these methods, see:
	http://stefaanlippens.net/python_profiling_with_pstats_interactive_mode