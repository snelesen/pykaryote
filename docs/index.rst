.. Pykaryote documentation master table of contents file
    sphinx-quickstart on Fri Jun 10 13:45:48 2011.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Pykaryote Documentation
=================================================
This documentation aims to be a comprehensive guide to Pykaryote, a computer model for simulating the evolution of biological complexity. It serves as a guide for both users and developers.

Tutorial
-----------

The tutorial introduces the Pykaryote model and describes how to install and
run Pykaryote.

.. toctree::
    :maxdepth: 2

    tut/intro.rst
    tut/installing.rst
    tut/running.rst
    tut/petri.rst
    tut/deprecated.rst

Developer Guide
---------------

This guide is for developers who wish to contribute to Pykaryote. It describes the tools and work flow associated with modifying and building Pykaryote.

.. toctree::
    :maxdepth: 2

    dev/building.rst
    dev/contents.rst
    dev/workflow.rst

Reference Manual
-----------------

This document describes the design of Pykaryote. It provides a detailed description of Pykaryote's organisms, their genomes and configuration options, as well as a high level description of the program design.

.. toctree::
    :maxdepth: 2

    ref/genome.rst
    ref/conf.rst
    ref/data-format.rst
    ref/other_config_files.rst
    ref/modules.rst
    

Indices and tables
--------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

