Modules
=============
This section contains documentation for individual python modules contained in Pykaryote.

Pykaryote.sim
-----------------

.. toctree::
    :maxdepth: 2

    sim/sim.rst
    sim/env.rst
    sim/org.rst
    sim/gen.rst
    sim/fam.rst
    sim/comp.rst

Pykaryote.petri
------------------
.. toctree::
    :maxdepth: 2
    
    petri.rst

Pykaryote.utils
----------------------
.. toctree::
    :maxdepth: 2

    utils/analysis.rst
    utils/globals.rst
    utils/comparison.rst

Pykaryote.test
------------------------
.. toctree::
    :maxdepth: 2

    test/test.rst