Comparer
================
This module handles comparison.

Documentation
--------------

.. automodule:: pykaryote.utils.comparison

.. autoclass:: Comparer
    :members:

.. autoclass:: AnalyzerAggregator
	:members:

.. autoclass:: Grapher
    :members:

.. autofunction:: get_config

.. autofunction:: make_cfg

.. autofunction:: get_metrics_files

