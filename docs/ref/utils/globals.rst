Globals
================
Overview
----------

The **globals** module is the bridge between configuration files and 
simulations. Before a :py:class:`Simulation` is created, **globals** must be initialized 
using :py:func:`init_globals`. 

For a listing of all global variables, see :ref:`vars`

Documentation
--------------

.. automodule:: pykaryote.utils.globals
