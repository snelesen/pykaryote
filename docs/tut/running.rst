Running Pykaryote
=======================================
This section describes Pykaryote's built in command line tools.

Setting Up Simulations
----------------------------
Simulations are controlled by settings in a configuration file. The default settings are in ``pykaryote/configs/master.cfg``. Do not edit this file. The default configuration file used by Pykaryote's command line tools is ``pykaryote/configs/sim.cfg``. You are free to make a copy of this file and edit it.

For more information on configuration files and detailed descriptions of each setting, see :ref:`config_files`.

Simulations and Comparisons
-----------------------------
The most basic usage of pykaryote is to run a single simulation. When a simulation is run, data is collected and saved. That data can then be analyzed and graphed.

Due to the random nature of the model, the results of a simulation vary from run to run, even when the same configuration options are used. For this reason, it is a good idea to run a simulation multiple times and then look at the average of those runs. Pykaryote includes tools to do so.

Research with Pykaryote involves exploring the results of different configuration options. With this in mind, Pykaryote has the ability to run different versions of a simulation where a setting is varied each run. This is called a comparison.

For instance, a comparison could be run with the default configuration file, but using values of ``0.0001``, ``0.001``, ``0.01``, and ``0.1`` for ``strong_chance``. Of course, comparisons can be averaged over multiple runs just like simulations.

Comparisons are configured using a separate configuration file. The default configuration file is located in ``pykaryote/configs/cmp.cfg``. For a description of the comparison configuration file format, see :ref:`cmp_cfg`.

Command Line Tools
------------------------
Pykaryote comes with several command line tools.

* petri
	* The main front end to pykaryote. Petri will automatically run, aggregate, and graph comparisons. Use it to run a single comparison on a single processor, or to run large batch jobs on many processors using MPI.
* pyk-test
	* Runs unit tests. Use it to test that Pykaryote is working correctly.
* pyk-profile
	* Profiles code and records statistics about how long Pykaryote takes to run. For developers only; regular users can ignore this tool.
* pyk-publication-graph
	* Creates black and white graphs for inclusion in a scientific paper. This tool expects a specific set of data files. For researchers at Calvin College only; regular users can ignore this tool.

Deprecated Scripts
--------------------
Pykaryote also ships with a number of deprecated scripts. They have been replaced by ``petri`` but are included for backwards compatibility.

* pyk-run
	* This most basic script. Runs a single simulation.
* pyk-analyze
	* Produces graphs of simulation data from pyk-run and pyk-mass-run.
* pyk-mass-run
	* Runs a simulation multiple times and averages the data.
* pyk-compare
	* Runs a comparison.
* pyk-comparison-graph
	* Produces graphs of a comparison.