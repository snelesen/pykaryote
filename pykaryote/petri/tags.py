#!/usr/bin/env python
"""Tags contains constants for tags used when communicating over MPI.
"""
MASTER_ID = 0
JOB_TAG = 1
SUCCESS_TAG = 2
FAILED_TAG = 3
SHUTDOWN_TAG = 4
STATUS_TAG = 5
